<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>php</title>
</head>
<body>
    <?php 
    // public,private,protected
    class Person{
        private $id;
        protected $name;
        public $age;
        protected $address;
        function _construct($id,$name,$age=20, $address=null){
             $this->id = $id;
             $this->name = $name;
             $this->age = $age;
             $this->address = $address;
        }
        private function speak(){
            echo 'hello world';
        }
    }
    class Student extends Person{
        var $score;
        function _construct($id, $name, $age, $address, $score){
            parent::_construct($id, $name, $age, $address);
            $this->score = $score;
        }
        function speak(){
            parent::speak();
            echo '<br/>';
            echo'I am a Student';
        }
    }
    class Address{
        var $city;
        var $country;
        function _construct($city,$country){
            $this->city = $city;
            $this->country = $country;
        }
    }
   ?>
   </body>
   </html>